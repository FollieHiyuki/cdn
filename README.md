# folliehiyuki's CDN

This is a small website, acting like a centralized source for all of my web assets. Most of them are vendored fonts and small CSS files.

The website is deployed to Cloudflare Pages and is available at <https://cdn.folliehiyuki.com>.

## FAQ

> Why not `<link href="url-to-original-source">`?

I like being in control of my own stuff. Also, it's because my websites have a strict `Content-Security-Policy` header value (see [_headers](./src/_headers) file for an example).

**NOTE**: to generate the integrity checksum, run: `openssl dgst -sha256 -binary <file_path> | openssl base64 -A`

> Why not using Chrome or Nginx directory listing?

I like some theming and little directory/file icons. Also, CloudFlare Pages (where I deploy this project to) isn't a VM hosting platform but a [FaaS](https://en.wikipedia.org/wiki/Function_as_a_Service) provider.

> Why not using `tree(1)`?

```bash
$ find ./out/ -mindepth 1 -type d -print -exec sh -c 'tree "$0" \
	-a \
	-i \
	-F \
	-H "." \
	-I "index.html" \
	-T "$0" \
	-L 1 \
	-o "$0/index.html" \
	--noreport \
	--dirsfirst \
	--charset utf-8 \
	--ignore-case' {} \;
```

Meh?

Also, `tree` injects its metadata, CSS and copyright notice into the `index.html` file. Yes, I can yank them out with something like [htmlq](https://github.com/mgdm/htmlq), but I also want to insert my own styling and stuff.

> Nicer way to have directory listing?

Yes, there are many better alternatives that exist like [serve](https://github.com/vercel/serve).

## License

All the vendored files are under their original licenses. Other are licensed under MIT.
