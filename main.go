package main

import (
	"flag"
	"fmt"
	"io/fs"
	"path/filepath"
	"strings"

	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/log"
	"github.com/tdewolff/minify/v2/css"
	"github.com/tdewolff/minify/v2/html"
)

func init() {
	// Being fancy ^-^
	logger.SetStyles(&log.Styles{
		Separator: lipgloss.NewStyle().Foreground(lipgloss.Color("#7b88a1")).Bold(true),
		Message:   lipgloss.NewStyle().Foreground(lipgloss.Color("#7b88a1")),
		Key:       lipgloss.NewStyle().Foreground(lipgloss.Color("4")).Bold(true),
		Levels: map[log.Level]lipgloss.Style{
			log.FatalLevel: lipgloss.NewStyle().
				Bold(true).
				Background(lipgloss.Color("5")).
				Foreground(lipgloss.Color("0")).
				Padding(0, 1, 0, 1).
				SetString("FATAL"),
			log.ErrorLevel: lipgloss.NewStyle().
				Bold(true).
				Background(lipgloss.Color("1")).
				Foreground(lipgloss.Color("0")).
				Padding(0, 1, 0, 1).
				SetString("ERROR"),
			log.InfoLevel: lipgloss.NewStyle().
				Bold(true).
				Background(lipgloss.Color("6")).
				Foreground(lipgloss.Color("0")).
				Padding(0, 1, 0, 1).
				SetString("INFO"),
		},
		Keys: map[string]lipgloss.Style{
			"err": lipgloss.NewStyle().Bold(true).Foreground(lipgloss.Color("1")),
		},
	})

	// Load minifiers
	minifier.AddFunc("text/html", html.Minify)
	minifier.AddFunc("text/css", css.Minify)
}

func main() {
	genFlag := flag.Bool("gen", false, "Generate index files")
	previewFlag := flag.Bool("preview", false, "Preview the built website")
	minifyFlag := flag.Bool("minify", false, "Minify existing CSS files")
	flag.Usage = func() {
		fmt.Printf(`Usage: go run . [OPTIONS] DIR

Arguments:
  DIR
	Path to the directory containing the source files

Options:
`)
		flag.PrintDefaults()
	}
	flag.Parse()

	// The root directory argument is required
	if len(flag.Args()) != 1 {
		logger.Fatal("failed to parse arguments", "err", "exactly 1 argument DIR is required")
	}

	rootDir, err := filepath.Abs(flag.Arg(0))
	if err != nil {
		logger.Fatal("unable to get root directory", "err", err)
	}

	// Run minification first, to ensure file size is calculated correctly later
	if *minifyFlag {
		err = filepath.WalkDir(rootDir, func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				return err
			}

			if d.Type().IsRegular() && filepath.Ext(path) == ".css" {
				logger.Info("minify CSS", "file", strings.TrimPrefix(path, rootDir+"/"))
				return minifyInPlace("text/css", path)
			}

			return nil
		})
		if err != nil {
			logger.Fatal("failed minifying CSS files", "err", err)
		}
	}

	if *genFlag {
		err = filepath.WalkDir(rootDir, func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				return err
			}
			return traverse(rootDir, path, d)
		})
		if err != nil {
			logger.Fatal("failed to traverse root directory", "err", err)
		}
	}

	// Only preview after things are generated
	if *previewFlag {
		logger.Fatal("failed to preview website", "err", preview(rootDir, previewPort))
	}
}
