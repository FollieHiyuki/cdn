{
  description = "folliehiyuki's stupid CDN";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      nixpkgs,
      flake-utils,
      gitignore,
      ...
    }:
    flake-utils.lib.eachSystem
      [
        "x86_64-linux"
        "x86_64-darwin"
        "aarch64-linux"
        "aarch64-darwin"
      ]
      (
        system:
        let
          inherit (nixpkgs) lib;
          pkgs = nixpkgs.legacyPackages."${system}";
        in
        {
          formatter = pkgs.nixfmt-rfc-style;

          packages = rec {
            default = bundle;

            cdn = pkgs.buildGoModule {
              name = "cdn";
              src = gitignore.lib.gitignoreSource ./.;
              vendorHash = "sha256-sNnP3Ue4+6f8Nw3jBbLdsfiqoUL2eXgZaYz1HdKOP/k=";
              ldflags = [
                "-s"
                "-w"
              ];
              preBuild = ''
                ${pkgs.templ}/bin/templ generate
              '';
            };

            bundle =
              with pkgs;
              stdenv.mkDerivation {
                name = "cdn.folliehiyuki.com";
                src = lib.cleanSource (gitignore.lib.gitignoreSource ./.);
                buildPhase = ''
                  ${cdn}/bin/cdn -minify -gen src
                '';
                installPhase = ''
                  mkdir -p "$out"
                  cp -r ./src/* "$out"/
                '';
              };
          };

          devShells.default =
            with pkgs;
            mkShellNoCC {
              name = "cdn";
              meta.description = "Development shell for folliehiyuki's cdn project.";
              packages = [
                just
                go
                golangci-lint
                nix-output-monitor
                nodePackages.wrangler
                openssl
                templ
                unzip
              ];
            };
        }
      );
}
