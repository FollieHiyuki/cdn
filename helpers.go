package main

import (
	"bytes"
	"context"
	"fmt"
	"io/fs"
	"math"
	"os"
	"path/filepath"
	"slices"
	"strings"

	"github.com/a-h/templ"
	"github.com/charmbracelet/log"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/tdewolff/minify/v2"
)

var (
	ctx         = context.Background()
	previewPort = ":8080"
	logger      = log.New(os.Stderr)
	minifier    = minify.New()
)

func closeFile(file *os.File) {
	if err := file.Close(); err != nil {
		logger.Fatal("failed to close file", "err", err)
	}
}

// minifyInPlace replaces the file content with the minified version
func minifyInPlace(mediatype, path string) error {
	tempPath := path + ".bak"
	err := os.Rename(path, tempPath)
	if err != nil {
		return err
	}

	inputFile, err := os.Open(tempPath)
	if err != nil {
		return err
	}

	// Open the file again for writing
	outputFile, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0o644)
	if err != nil {
		return err
	}

	defer func() {
		closeFile(inputFile)
		closeFile(outputFile)

		if e := os.Remove(tempPath); e != nil {
			logger.Fatal("failed to delete temporary input file", "file", path)
		}
	}()

	return minifier.Minify(mediatype, outputFile, inputFile)
}

func genIndexFile(path string, c templ.Component) error {
	indexFile, err := os.Create(filepath.Join(path, "index.html"))
	if err != nil {
		return err
	}
	defer closeFile(indexFile)

	buf := new(bytes.Buffer)
	if e := c.Render(ctx, buf); e != nil {
		return e
	}

	return minifier.Minify("text/html", indexFile, buf)
}

func traverse(prefix, path string, d fs.DirEntry) error {
	// Generate home page for the root directory.
	if prefix == path {
		logger.Info("create index.html for root directory")
		return genIndexFile(path, indexPage())
	}

	// Otherwise generate a listing page
	if d.IsDir() {
		trimmedPath := strings.TrimPrefix(strings.TrimPrefix(path, prefix), "/")
		logger.Info("create index.html", "dir", trimmedPath)

		entries, err := os.ReadDir(path)
		if err != nil {
			return err
		}

		// List directories first
		slices.SortStableFunc(entries, func(a, b fs.DirEntry) int {
			if a.IsDir() && !b.IsDir() {
				return -1
			}

			if !a.IsDir() && b.IsDir() {
				return 1
			}

			return strings.Compare(a.Name(), b.Name())
		})

		return genIndexFile(path, listingPage(trimmedPath, entries))
	}

	return nil
}

// preview Previews the website at specified root directory on localhost:<port>
func preview(root, port string) error {
	e := echo.New()
	e.Use(middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
		LogURI:     true,
		LogError:   true,
		LogStatus:  true,
		LogMethod:  true,
		LogLatency: true,
		LogValuesFunc: func(_ echo.Context, v middleware.RequestLoggerValues) error {
			logger.
				With("Method", v.Method).
				With("URI", v.URI).
				With("Status", v.Status).
				With("Latency", v.Latency).
				Info("request")

			if v.Error != nil {
				logger.Error("request", "err", v.Error)
			}
			return nil
		},
	}))
	e.Static("/", root)
	return e.Start(port)
}

// copied from github.com/dustin/go-humanize. I only need 1 function from the module.
func humanateBytes(s int64) string {
	base := 1000.0
	sizes := []string{"B", "kB", "MB", "GB", "TB", "PB", "EB"}

	if s < 10 {
		return fmt.Sprintf("%d B", s)
	}

	e := math.Floor(math.Log(float64(s)) / math.Log(base))
	suffix := sizes[int(e)]

	val := math.Floor(float64(s)/math.Pow(base, e)*10+0.5) / 10

	f := "%.0f %s"
	if val < 10 {
		f = "%.1f %s"
	}

	return fmt.Sprintf(f, val, suffix)
}
