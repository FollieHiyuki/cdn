set shell := ["/usr/bin/env", "bash", '-c']

default: lint build

gen:
	@templ generate

lint: gen
	@golangci-lint run

# INFO: `nom build .#bundle` can also be invoked as `nix build .#bundle |& nom`
build:
	nom build .#bundle

preview: build
	@nix run .#cdn -- -preview result/

sri-hashes bits='384':
	@find ./result/ \
		-type f -name "*.woff" -or -name "*.woff2" -or -name "*.css" |\
		while read -r file; do \
		echo "{{BOLD + GREEN}}>>>{{NORMAL + BOLD}} $(echo "$file" | sed 's@^./result/@@g'):{{NORMAL}} sha{{bits}}-$(openssl dgst -sha{{bits}} -binary "$file" | openssl base64 -A)"; \
	done

publish: build
	@wrangler pages deploy \
		--project-name=folliehiyuki-cdn \
		--branch="${CI_COMMIT_REF_NAME:-$(git branch --show-current)}" \
		result/

css-normalize:
	@echo "{{BOLD + GREEN}}>>>{{NORMAL + BOLD}} Downloading {{YELLOW}}normalize.css{{NORMAL}}"
	@curl -#fL \
		-o ./src/styles/normalize.css \
		https://github.com/necolas/normalize.css/raw/master/normalize.css

css-modern-normalize:
	@echo "{{BOLD + GREEN}}>>>{{NORMAL + BOLD}} Downloading {{YELLOW}}modern-normalize.css{{NORMAL}}"
	@curl -#fL \
		-o ./src/styles/modern-normalize.css \
		https://github.com/sindresorhus/modern-normalize/raw/main/modern-normalize.css

css-sakura:
	@mkdir -p ./src/styles/sakura
	@for variant in '-dark' ''; do \
		echo "{{BOLD + GREEN}}>>>{{NORMAL + BOLD}} Downloading {{YELLOW}}sakura${variant}.css{{NORMAL}}"; \
		curl -#fL \
			-o ./src/styles/sakura/sakura${variant}.css \
			https://github.com/oxalorg/sakura/raw/master/css/sakura${variant}.css; \
	done

css-pure version='3.0.0':
	#!/usr/bin/env bash
	cdn_url="https://cdn.jsdelivr.net/npm/purecss@{{version}}/build"
	mkdir -p ./src/styles/pure
	for variant in buttons forms grids menus tables pure grids-responsive; do
		echo "{{BOLD + GREEN}}>>>{{NORMAL + BOLD}} Downloading {{YELLOW}}${variant}.css{{NORMAL}} variant of pure-css"
		curl -#fL -o ./src/styles/pure/${variant}.css ${cdn_url}/${variant}.css
	done

css-hint:
	@mkdir -p ./src/styles/hint
	@for variant in '.base' ''; do \
		echo "{{BOLD + GREEN}}>>>{{NORMAL + BOLD}} Downloading {{YELLOW}}hint${variant}.css{{NORMAL}}"; \
		curl -#fL \
			-o ./src/styles/hint/hint${variant}.css \
			https://github.com/chinchang/hint.css/raw/master/hint${variant}.css; \
	done

font-devicon:
	@mkdir ./src/fonts/devicon
	@echo "{{BOLD + GREEN}}>>>{{NORMAL + BOLD}} Downloading {{YELLOW}}devicon.woff{{NORMAL}}"
	@curl -#fL \
		-o ./src/fonts/devicon/devicon.woff \
		https://github.com/devicons/devicon/raw/master/fonts/devicon.woff

font-logos version='1.1.1':
	@echo "{{BOLD + GREEN}}>>>{{NORMAL + BOLD}} Downloading {{YELLOW}}font-logos.woff2{{NORMAL}}"
	@curl -#fL \
		-o /tmp/font-logos.zip \
		https://github.com/lukas-w/font-logos/releases/download/v{{version}}/font-logos-{{version}}.zip
	@unzip /tmp/font-logos.zip -d /tmp
	@mkdir -p ./src/fonts/font-logos
	@cp -fv /tmp/font-logos-{{version}}/assets/font-logos.woff2 ./src/fonts/font-logos/font-logos.woff2

font-awesome version='6.5.1':
	@echo "{{BOLD + GREEN}}>>>{{NORMAL + BOLD}} Downloading {{YELLOW}}FontAwesome Free{{NORMAL}}"
	@curl -#fL \
		-o /tmp/font-awesome.zip \
		https://github.com/FortAwesome/Font-Awesome/releases/download/{{version}}/fontawesome-free-{{version}}-web.zip
	@unzip /tmp/font-awesome.zip -d /tmp
	@mkdir -p ./src/fonts/font-awesome
	@for variant in brands-400 solid-900; do \
		cp -fv /tmp/fontawesome-free-{{version}}-web/webfonts/fa-${variant}.woff2 ./src/fonts/font-awesome/; \
	done

font-iosevka version='28.0.1':
	@mkdir -p ./src/fonts/iosevka
	@for variant in Iosevka IosevkaAile; do \
		echo "{{BOLD + GREEN}}>>>{{NORMAL + BOLD}} Downloading {{YELLOW}}${variant} Web{{NORMAL}} font"; \
		curl -#fL \
			-o /tmp/${variant}.zip \
			https://github.com/be5invis/Iosevka/releases/download/v{{version}}/PkgWebFont-${variant}-{{version}}.zip; \
		unzip /tmp/${variant}.zip -d /tmp; \
		for style in Bold BoldItalic Italic Regular; do \
			cp -fv /tmp/WOFF2/${variant}-${style}.woff2 ./src/fonts/iosevka/${variant,,}-${style,,}.woff2; \
		done; \
	done
	@for style in bold bolditalic italic regular; do \
		mv -fv ./src/fonts/iosevka/iosevkaaile-${style}.woff2 ./src/fonts/iosevka/iosevka-aile-${style}.woff2; \
	done

css: css-normalize css-modern-normalize css-hint css-pure css-sakura

fonts: font-logos font-devicon font-awesome font-iosevka

resources: css fonts
